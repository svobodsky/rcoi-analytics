cd ..\..

[Reflection.Assembly]::LoadWithPartialName("System.Web")
$POSTGRES_PASSWORD = ([System.Web.Security.FormsAuthentication]::HashPasswordForStoringInConfigFile([System.Web.Security.Membership]::GeneratePassword(32,0), "MD5")) 
$REDASH_COOKIE_SECRET = ([System.Web.Security.FormsAuthentication]::HashPasswordForStoringInConfigFile([System.Web.Security.Membership]::GeneratePassword(32,0), "MD5")) 
$REDASH_SECRET_KEY = ([System.Web.Security.FormsAuthentication]::HashPasswordForStoringInConfigFile([System.Web.Security.Membership]::GeneratePassword(32,0), "MD5")) 
$ENV_FILE = "env"

echo PYTHONUNBUFFERED=0 | Out-File -Encoding oem -FilePath $ENV_FILE
echo REDASH_LOG_LEVEL=INFO | Out-File -Encoding oem -FilePath $ENV_FILE -Append
echo REDASH_REDIS_URL=redis://redis:6379/0 | Out-File -Encoding oem -FilePath $ENV_FILE -Append
echo POSTGRES_PASSWORD=$POSTGRES_PASSWORD | Out-File -Encoding oem -FilePath $ENV_FILE -Append
echo REDASH_COOKIE_SECRET=$REDASH_COOKIE_SECRET | Out-File -Encoding oem -FilePath $ENV_FILE -Append
echo REDASH_SECRET_KEY=$REDASH_SECRET_KEY | Out-File -Encoding oem -FilePath $ENV_FILE -Append
echo REDASH_DATABASE_URL=postgresql://postgres:$POSTGRES_PASSWORD@postgres/postgres | Out-File -Encoding oem -FilePath $ENV_FILE -Append

