cd ../..
python -m venv python_venv
python_venv\Scripts\pip install --upgrade redash-toolbelt
python_venv\Scripts\redash-migrate.exe init
python_venv\Scripts\redash-migrate.exe users
python_venv\Scripts\redash-migrate.exe groups
python_venv\Scripts\redash-migrate.exe data-sources
python_venv\Scripts\redash-migrate.exe queries
python_venv\Scripts\redash-migrate.exe visualizations
python_venv\Scripts\redash-migrate.exe dashboards
