#!/bin/sh
cd ../..
POSTGRES_PASSWORD=$(gpg --gen-random --armor 1 24)
REDASH_COOKIE_SECRET=$(gpg --gen-random --armor 1 24)
REDASH_SECRET_KEY=$(gpg --gen-random --armor 1 24)
REDASH_DATABASE_URL=postgresql://postgres:${POSTGRES_PASSWORD}@postgres/postgres

echo "PYTHONUNBUFFERED=0" >> env
echo "REDASH_LOG_LEVEL=INFO" >> env
echo "REDASH_REDIS_URL=redis://redis:6379/0" >> env
echo "POSTGRES_PASSWORD=$POSTGRES_PASSWORD" >> env
echo "REDASH_COOKIE_SECRET=$REDASH_COOKIE_SECRET" >> env
echo "REDASH_SECRET_KEY=$REDASH_SECRET_KEY" >> env
echo "REDASH_DATABASE_URL=$REDASH_DATABASE_URL" >> env
