sudo apt install python3-pip
sudo pip3 install virtualenv 
cd ../..
virtualenv python_venv
python_venv/bin/pip3 install --upgrade redash-toolbelt
python_venv/bin/redash-migrate init
python_venv/bin/redash-migrate users
python_venv/bin/redash-migrate groups
python_venv/bin/redash-migrate data-sources
python_venv/bin/redash-migrate queries
python_venv/bin/redash-migrate visualizations
python_venv/bin/redash-migrate dashboards
